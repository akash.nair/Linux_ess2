#include "Helloclient.h"

int main(int argc, char const *argv[0])
{
    struct sockaddr_in client_addr;
    struct sockaddr_in server_addr; 
    char buffer[1024] = {0};
    char *message = "CLIENT WAVINGGG\n";

//  int socket(int domain, int type, int protocol);
    int socket_fd, dial, addr_verification;
    socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    
    if(socket_fd == -1) {
        printf("SOCKET ERROR\n");
        exit(0);
    }

    memset(&server_addr, 0, sizeof(server_addr));

    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(PORT);
        
/*  inet_pton - convert IPv4 and IPv6 addresses from text to binary form  
     int inet_pton(int af, const char *src, void *dst)                  */
    
    addr_verification = inet_pton(AF_INET, "198.021.123.111", &server_addr.sin_addr);
    if(addr_verification == -1) {
        printf("Invalid addr\n");
        exit(0);
    }

/*           int connect(int sockfd, const struct sockaddr *addr,
                   socklen_t addrlen)                           */

    dial = connect(socket_fd, (struct sockaddr *)&server_addr, sizeof(server_addr));
    if(dial == -1) {
        printf("ERROR IN CONNECTING\n");
        exit(-1);
    }
while(1) {
    
/* ssize_t send(int sockfd, const void *buf, size_t len, int flags) */
    send(socket_fd, message, strlen(message), 0);
    printf("Client message sent\n");
/*ssize_t read(int fd, void *buf, size_t count); */
    read(socket_fd, buffer, 1024);
    printf("%s", buffer);
    
    if(argv[0] == "quit") {
        exit(0);
    }
}
}

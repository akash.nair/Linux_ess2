
#include "P2server.h"

#define PORT 8080

int main(int argc, char **argv)
{
    int setsock_val, socket_fd, read_value, fd, sent, addrlen; 
    struct sockaddr_in server_addr, client_addr;
    struct dirent *read_dir;
    DIR *dirp;
    char arr[50] = "/home/akash/", original_array[50];
    
    socket_fd = socket(AF_INET, SOCK_STREAM, 0);

    if(socket_fd == 0) {
        printf("Socket failed\n");
        exit(0);
    }

    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_port = htons(PORT);
    bzero(&server_addr.sin_zero, 8);
    
    if(bind(socket_fd, (struct sockaddr *)&server_addr,  sizeof(server_addr))<0)
    {                                                                                               
         perror("bind failed");                                                                      
         exit(EXIT_FAILURE);                                                                         
    }  

    if( listen(socket_fd, 3) < 0) {
        printf("Error listen\n");
        exit(0);
    }

    strcpy(original_array, arr);
    
    while(1) {
        fd = accept(socket_fd, (struct sockaddr*)&server_addr,
                                    (socklen_t*)&addrlen);
        if(fd == -1) {
            printf("Error in Accept func\n");
            exit(0);
        }
        printf("NEW CLIENT PORT NUMBER:-%d HAVING IP :-%s\n",
                            ntohs(client_addr.sin_port),inet_ntoa(client_addr.sin_addr));
    
        
/* struct dirent *readdir(DIR *dirp);

 The opendir() function opens a directory stream corresponding to the directory name, and 
 * returns a pointer to the directory stream.The stream is positioned at the first entry in 
 * the directory. */
/* DIR *opendir(const char *name); */
        
        
        dirp = opendir("/home/akash");
        if(dirp == NULL) {
            perror("DIRECTORY OPENINIG ERROR\n");
            exit(0);
        }

        printf("LIST OF DIRECTORIES......\n");
        
        while(read_dir = readdir(dirp) != NULL) {
            printf("NAME OF FILE IS %s\n", read_dir->d_name); /*MEMBER OF STRUCT:char d_name[256]*/
            send(fd, read_dir->d_name, sizeof(read_dir->d_name), 0);
        }
 
        
        

    }
}

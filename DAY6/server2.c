#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<unistd.h>
#include<string.h>
#include<netinet/in.h>
#include<dirent.h>

#define PORT 8080

int main() 
{
    int setsock_val, socket_fd, read_value, fd, sent; 
    int addrlen;
    struct dirent *read_dir;
    char buffer[1024] = {0}; 
    int optval = 1;
    struct sockaddr_in server_addr;
    addrlen = sizeof(struct sockaddr_in);
    char *message = "HELLO THIS IS SERVER";
    
/* create a Socket file descriptor
   int socket(int domain, int type, int protocol); */
    socket_fd = socket(AF_INET, SOCK_STREAM, 0);

    if(socket_fd == 0) {
        printf("Socket failed\n");
        exit(0);
    }

 /*    int setsockopt(int sockfd, int level, int optname,
                           const void *optval, socklen_t optlen); */
    setsock_val = setsockopt(socket_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEADDR, &optval,
                                                                         sizeof(optval));
    if(setsock_val == -1) {
        printf("FAILURE SET SOCK\n");
        exit(0);
    }
   
   server_addr.sin_family = AF_INET;
   server_addr.sin_addr.s_addr = INADDR_ANY;
   server_addr.sin_port = htons(PORT);

/* FORM A UNIQUE ADDRESS FROM FORCEFULL ATTACHMENT OF SOCKET TO PORT
    int bind(int sockfd, const struct sockaddr *addr, socklen_t addrlen); 
 Assigns a unique address and attaches IP as well as PORT to our socket

    if(bind(socket_fd, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0) {
        printf("Error in bind\n");
        exit(0);
    }
*/

     if (bind(socket_fd, (struct sockaddr *)&server_addr,  sizeof(server_addr))<0) {                      
         perror("bind failed");                                                                      
         exit(EXIT_FAILURE);                                                                         
     }  

/* WAITS FOR A CALLER
            int listen(int sockfd, int backlog); */
    if( listen(socket_fd, 3) < 0) {
        printf("Error listen\n");
        exit(0);
    }

/* RECEIVE A CALL
    int accept(int sockfd, struct sockaddr *addr, socklen_t *addrlen); */
    fd = accept(socket_fd, (struct sockaddr*)&server_addr,
                                    (socklen_t*)&addrlen);
    if(fd == -1) {
        printf("Error in Accept func\n");
        exit(0);
    }

while(1) {

    DIR *dir_open = opendir("/home/akash/"); 

    if (dir_open == NULL) {
        perror("DIRECTORY OPENING FAIL\n");
        exit(0);
    }
    while(read_dir = readdir(dir_open) != NULL) {
        printf("FILE NAME : %s\n", read_dir->d_name);
    }
    closedir(dir_open);

/*  ssize_t read(int fd, void *buf, size_t count); */
    read_value = read(fd, buffer, 1024);
    printf("%s", buffer);

/*  ssize_t send(int sockfd, const void *buf, size_t len, int flags); */
    sent = send(fd, message, sizeof(message), 0);
    printf("MESSAGE SENT FROM SERVER SIDE\n");


}
    return 0;

}

/**************************************************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file	 Ex2.c
 * @brief:	Develop a parent-child process application, wherein child process prints the 
		number from 1 to 10000 and parent process waits for child process to complete.
 *
 * @Author       - Akash Nair
 * @bug		     - no bugs
 ***************************************************************************************************
 *
 * History
 *
 *                      
 * MAY/02/2018, Akash , Created (description)
 *
 **************************************************************************************************/
#include "Ex1.2.h"
int main ()
{
	pid_t child_pid;
	printf ("the main program process ID is %d\n", (int) getpid ());
	child_pid = fork ();
	
	int i;
	if (child_pid != 0) {
		printf ("Parent process : %d\n", (int) getpid ());
		printf ("Id  of child :  %d\n", (int) child_pid);
	}
	else {
		printf ("child process, with id %d\n", (int) getpid ());
		for(i = 1; i <= 1000; i++) {
			printf("%d ", i);	
		}
	}
	printf("\n");
	return 0;
}


/*****************************************************************************
 * @file EX1_2.h
 * @brief (Function prototype for child process application)
 * 
 *     Develop a parent-child process application, wherein child process prints
 *    the number from 1 to 10000 and parent process waits for child process to
 *    complete.
 * @author Akash Nair
 * @bug No known bugs.
 *****************************************************************************/

/*****************
 *Includes
********************/ 

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>


/**************************
 *Defines
 **********************/

 
/****************************
* GLOBAL VARIABLES
* ************************/

/***************************
 * Function Prototype
 **************************/


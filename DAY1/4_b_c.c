
/**************************************************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file	 4_b_c.c
 * @brief    Write a C program that communicates between child and parent
 *           processes using linux signals.
 *           a. Create a child process from the parent process.
 *           b. Parent will send messages to child using any different thress linux
 *           signals.
 *           c. Child needs to pick up these signals and calls appropriate functions for
 *           printing conformation on screen
 *
 *
 * @Author       - Akash Nair
 * @bug		     - no bugs
 ***************************************************************************************************
 *
 * History
 *
 *                      
 * MAY/04/2018, Akash , Created (description)
 *
 **************************************************************************************************/

#include "4_b.h"

void myhandler(int sig)
{
    printf("-----INTERRUPT RECEIVED----\n");
    printf("CHILD WAS INTERRUPTED\n");  
}

int main()
{
  pid_t PROCESS;
  PROCESS = fork();
  int status;
  switch(PROCESS) {

    case -1:
    printf("ERROR IN FORKING\n");
    break;

    case 0:
    printf("\n\nCHILD PROCESS CREATED......\n");
    signal(SIGINT, myhandler);
    printf("*************Waiting for INTR*****************\n");
    pause();
    sleep(2);
    break;

    default:
    printf("PARENT PROCESS CREATED.......\n");
    printf("\nSENDING SIGNAL TO CHILD\n");
    sleep(5);
    kill(PROCESS, SIGINT);
    PROCESS = wait( &status );
    break;
  }

    return 0;
}

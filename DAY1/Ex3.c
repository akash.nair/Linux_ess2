/**************************************************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file	 Ex1.3.c
 * @brief:	Write a C program that will create a child process which is a simple copy of the 
		parent process and each should report their existence by outputting its own PID and its
		PPID to the screen. Modify the program so that the child process creates its own child process which 
		also reports its own and its parents PID. 

 * @Author       - Akash Nair
 * @bug		     - no bugs
 ***************************************************************************************************
 *
 * History
 *
 *                      
 * MAY/01/2018, Akash , Created (description)
 *
 **************************************************************************************************/

#include "Ex1.3.h"
int main()
{
	pid_t PROCESS1, PROCESS2;
	PROCESS1 = fork();
	
	if(PROCESS1 == 0) {
		printf("In child Process\n");
		printf("Value of a is %d\n", (int) PROCESS1);
		printf("PID in child Process : %d\n", (int) getpid());
		printf("PPID IN CHILD PROCESS : %d\n", (int) getppid());

		PROCESS2 = fork();	
		if(PROCESS2 == 0) {
			printf("Inside child of B (B's child)\n");
			printf("Value of B : %d\n", (int) PROCESS2);
			printf("PID inside child Process of A : %d\n", (int) getpid());
			printf("PPID INSIDE A's CHILD PROCESS : %d\n", (int) getppid());
		}

		else if(PROCESS2 != 0){
			printf("Inside parent of B\n");
			printf("PID of B is %d\n", (int) getpid());
			printf("(This should be A's child's PID)PPID  of B is : %d\n", (int) getppid());
		}
		
	
		else {
			 printf("Inside A's parent:%d\n", (int)getpid());
			 printf("PID in Parent Process of A : %d\n", (int) getppid());
		}

}
}

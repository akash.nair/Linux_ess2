#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>

void alarm_handler(int s);

int i = 0;

/*ALARM HANDLER*/

void alarm_handler(int s)
{
		printf("Inside alarm handler function\n");
		alarm(5);
}

int main()
{
	printf("PID of main process: %d\n", (int) getpid());
	pid_t pid = fork();

/*FORK ERROR*/
	
	if(pid < 0){
		perror("Fork failed\n");
	}

/*CHILD PROCESS*/
	
	if(pid == 0) {
		printf("Inside child process\n");	
	}	

/*PARENT PROCESS*/
	
	if(pid > 0) {
		printf("Inside parent\n");

		/*SENDING SIGNAL*/		
		signal(SIGALRM, alarm_handler);
		alarm(4);	/*ALARM IN A GAP OF 4 SECONDS*/
		for(i = 1; i < 5; i++) {
			printf("PROCESS IN PARENT\n");
			pause();
		}
	}
	return 0;		
}

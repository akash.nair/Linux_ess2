/*****************************************************************************
 * @file EX4_b_c.h
 * @brief (Function prototype for child process application)
 * 
 *     Develop a parent-child process application, wherein child process prints
 *    the number from 1 to 10000 and parent process waits for child process to
 *    complete.
 * @author Akash Nair
 * @bug No known bugs.
 *****************************************************************************/

/*****************
 *Includes
********************/ 
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>

/**************************
 *Defines
 **********************/

 
/****************************
* GLOBAL VARIABLES
* ************************/

/***************************
 * Function Prototype
 **************************/


/*********************************************************************
 *@brief (This function to handle all signals in function.) 
 *
 *@param int (sig)
 *
 *@return (void is returned back)
 *******************************************************************/

void myhandler(int sig);



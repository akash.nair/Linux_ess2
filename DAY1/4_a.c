/**************************************************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file	 Ex3_1.c
 * @brief:	Develop a program to construct a binary search tree.
 *		    information to understand difference between different types of search.
 *          Develop a multi-threaded application to perform 3X3 matrix
 *          multiplication, each cell of resultant matrix needs to be
 *          calculated in separate thread.
 *
 *
 * @Author       - Akash Nair
 * @bug		     - no bugs
 ***************************************************************************************************
 *
 * History
 *
 *                      
 * MAY/02/2018, Akash , Created (description)
 *
 **************************************************************************************************/

#include "4_a.h"

int main()
{
    pid_t PROCESS, PROCESS_PARENT;
    PROCESS = fork();
    
    if(PROCESS < 0) {
      printf("ERROR IN FORKING()\n"); 
    }

    if(PROCESS == 0){
      printf("THIS IS A CHILD PROCESS\n"); 
    }
 
    if(PROCESS > 0){
      printf("THIS IS A PARENT PROCESS\n"); 
      PROCESS_PARENT = fork();
      if(PROCESS_PARENT == 0) {
        printf("THIS IS CHILD CREATED BY PARENT PROCESS\n");
      }
    }   
}

/***************************************************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file	 P3.c
 * @brief:	 Develop a parent-child process application, wherein child
 *           process at random duration calculates square of numbers (via above method)
 *           from 1 to 1000 and parent process prints result as soon as soon as child
 *           process produce it. Use Shared-memory for IPC

 * @Author       - Akash Nair
 * @bug		     - no bugs
 ***************************************************************************************************
 *
 * History
 *
 *                      
 * MAY/15/2018, Akash , Created (description)
 *
 **************************************************************************************************/
#include "P3.h"

int main()
{
    pid_t PROCESS;
    int shmid, status, return1, num, loop1;

    sem_init(&mutex, 1, 2);
    key_t key = ftok("shmfile", 65);
    shmid = shmget(key, 4096, 0666|IPC_CREAT);
    str = (int *)shmat( shmid, (const void*)0, 0);

    PROCESS = fork();

    if(PROCESS > 0) {
        sleep(1);
        for(loop1 = 1; loop1 < 1000; loop1++) {
            sem_wait(&mutex);
            printf("\nSquare of given number is %s\n", (char *)str);
            sleep(1);
            sem_post(&mutex);
        }
        waitpid(PROCESS, &status, 3);
        return1 = shmdt(str);
        printf("RETURN VALUE : %d\n", return1);
    }
    else {
        
        printf("Inside child\n");
        int loop2, loop;
        for(loop2 = 0; loop2 < 1000; loop2++) {
            sem_wait(&mutex);
            int sum = loop2, loop;
            for(loop = 1; loop < loop2; loop++) {
                sum = sum + loop2;
            }
            sprintf(str, "%d", sum);
            sem_post(&mutex);
            sleep(1);
            }
        }
//        sem_destroy(&mutex);
    return 0;
}

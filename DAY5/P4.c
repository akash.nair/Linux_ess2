/***************************************************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file	 P4.c
 * @brief:	 Develop a simple C based shell, which continuously scans for
 *           user input and accordingly execute commands. This shell should validate user
 *           inputs. Extra: How would you extend this shell so it provides “|” (Linux
 *           pipe) feature?
 *
 * @Author       - Akash Nair
 * @bug		     - no bugs
 ***************************************************************************************************
 *
 * History
 *
 *                      
 * MAY/15/2018, Akash , Created (description)
 *
 **************************************************************************************************/

#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<sys/types.h>
#include<stdlib.h>
#include<sys/wait.h>
#include<unistd.h>

#define MAX_CHAR 10

int main()
{
    int status;
    pid_t PROCESS;

    char command[MAX_CHAR];
    char *string;

    while(1) {
        printf("akash@akash$ ");

        string = fgets(command, sizeof(command), stdin );
        if (string == NULL) exit(-1);
        command[ strlen(command)-1] = 0;

        if (!strncmp(command, "exit", 3)) exit(0);
        PROCESS = fork();

        if (PROCESS == 0) {
            execlp( command, command, NULL );
        } 
        else if (PROCESS > 0) {
            waitpid(PROCESS, &status, 0 );
        }
        printf("\n");
    }
    return 0;
}

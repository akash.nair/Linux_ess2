#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>

int main()
{
    int socket_fd, connect_fd, read_bytes;
    char buffer[256];
	struct sockaddr_in server_addr;

    socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    memset(buffer, '0', sizeof(buffer));

    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = inet_addr("127.0.0.1"); /* REFERS TO 0.0.0.0 */
    server_addr.sin_port = htons(5000);     /*This might be in network byte order  */

    /*  int connect(int sockfd, const struct sockaddr *addr,
                        socklen_t addrlen);
    */
    connect_fd = connect(socket_fd, (struct sockaddr *)&server_addr, sizeof(server_addr));
    
    if(connect_fd == -1) {
        perror("CONNECT ERROR\n");
        return 1;
    }

    FILE *fp; 
    fp = fopen("/home/akash/Linux_ess2/Sample.txt", "ab");

    if(fp == NULL) {
        perror("ERROR IN OPENING FILE\n");
        return 1;
    }
    /* READ DATA IN CHUNKS */
    read_bytes = read(socket_fd, buffer, 256);
    while(read_bytes > 0) {
        if(read_bytes > 0) {
            printf("Bytes received %d\n", read_bytes);
            fwrite(buffer, 1, read_bytes, fp);
        }
    
        if(read_bytes < 0) {
            printf("Error in reading\n");
        }
    
        return 0;
    }

}


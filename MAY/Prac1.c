#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<errno.h>
#include<string.h>
#include<unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>

int main()
{
	int socket_fd, bind_fd, listen_fd, accept_fd, data_read;
	struct sockaddr_in server_addr;
	char buffer[1024];

	socket_fd = socket(AF_INET, SOCK_STREAM, 0);

	if(socket_fd == -1) {
		perror("SERVER FILE DESCRIPTOR\n");
		exit(-1);
	}
	
    memset(&server_addr, '0', sizeof(server_addr));
    memset(buffer, '0', sizeof(buffer));

    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY);/* REFERS TO 0.0.0.0 */
    server_addr.sin_port = htons(5000);     /*This might be in network byte order  */
	
/* BIND A ADDRESS TO A SOCKET */

    bind_fd = bind(socket_fd, (struct sockaddr *)&server_addr, sizeof(server_addr));
    
    if(bind_fd == -1) {
        perror("BINDING ERROR\n");
        exit(-1);
    }

/* LISTEN FOR CONNECTIONS ON SOCKET OR WAITING FOR A CALL */

    listen_fd = listen(socket_fd, 10);

    if(listen_fd == -1) {
        perror("LISTEN ERROR\n");
        exit(-1);
    }

/* OPEN A FILE TO SEND */

    while(1) {

/* ACCEPT A CONNECTION */
        accept_fd = accept(listen_fd, (struct sockaddr*)NULL, NULL);
        FILE *fp; 
        fp = fopen("/home/akash/Linux_ess2/E.txt", "rb");

        if(fp == NULL) {
            perror("ERROR IN OPENING FILE\n");
            return 1;
        }
    
/*READ DATA FROM THAT FILE AND SEND IT*/
    
        while(1) {
            unsigned char buffer2[4096] = {0};
            data_read = fread(buffer2, 1, 4096, fp);
            printf("\nBYTES READ %d \n", data_read);

/*IF READ IS SUCCESSFUL WRITE DATA(SEND DATA) */

            if(data_read > 0) {
                printf("Sending data..\n");
                write(accept_fd, buffer2, data_read);
            }

            if(data_read < 256) {
                if(feof(fp)) {
                    printf("End of File\n");
                }

                if(ferror(fp)) {
                    printf("Error in opening file\n");
                }
//                perror("DATA_READ \n");
            }
        }

        close(accept_fd);
        sleep(1);
    }
    return 0;
}

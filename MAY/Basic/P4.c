#include<stdio.h>
#include<pthread.h>
#include<unistd.h>
#include<errno.h>
#include<ctype.h>

#define NUM_LOOP 100000

int sum = 0;

pthread_mutex_t mymutex = PTHREAD_MUTEX_INITIALIZER;


void* counting_function(void *arg)
{
    int value = *(int *)arg; //type casting and de-referencing
    int loop1;
    for(loop1 = 0; loop1 < NUM_LOOP; loop1++) {

//CRITICAL SECTION//
//MUTEX STARTING POINT
        pthread_mutex_lock(&mymutex);
            sum += value; 
//MUTEX ENDING POINT
            pthread_mutex_unlock(&mymutex);
        }
    }

    int main()
    {
        int value1 = 1;
    pthread_t ID1;
    pthread_create(&ID1, NULL, counting_function, &value1);

    int value2 = -1;
    pthread_t ID2;
    pthread_create(&ID2, NULL, counting_function, &value2);

//    pthread_join(ID1, NULL);
 //   pthread_join(ID2, NULL);
    
    printf("%d\n", sum);
    return 0;    
}

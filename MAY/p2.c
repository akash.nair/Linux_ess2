#include <pthread.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <ctype.h>

//Thread function to create sum of N numbers

struct sum_run 
{
    int limit;
    int answer;
};

void* sum_runner(void *arg)
{
    int i;
    struct sum_run *arg_ptr = (struct sum_run*) arg;
 
    int sum = 0;
    for(i = 0; i <= (arg_ptr->limit); i++){
        sum += i;
    }

    arg_ptr->answer = sum;

    pthread_exit(0);
}


int main(int argc, char **argv)
{
    int i;
    if(argc < 2) {
        printf("error %s \n", argv[0]);
        exit(-1);
    }

    int num_args = argc - 1;
    struct sum_run args[num_args];
    
    // Thread id
    pthread_t tid[num_args];

    // Thread attribute
    for(i = 0; i < num_args; i++) {
        args[i].limit = atoi(argv[i + 1]);
    
        pthread_attr_t attr;
        pthread_attr_init(&attr);
        pthread_create(&tid[i], &attr, sum_runner, &args[i]); //sum_runner is a function ptr
    }

    // wait untill thread is done
    for(i = 0; i < num_args; i++) {
        pthread_join(tid[i], NULL);
        printf("Sum is %d\n", args[i].answer);
    }
}

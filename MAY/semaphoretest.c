#include<stdio.h>
#include<unistd.h>
#include<semaphore.h>
#include<pthread.h>


sem_t mutex;



void* threadfunc(void* arg)
{
    sem_wait(&mutex);
    printf("Inside ....function\n");

    //CRITICAL SEC
    sleep(4);

    printf("\nExiting....function\n");
    sem_post(&mutex);


}

int main()
{
    sem_init(&mutex, 0, 1);
    pthread_t t1, t2;
    
    pthread_create(&t1, NULL, threadfunc, NULL);
    sleep(2);
    pthread_create(&t2, NULL, threadfunc, NULL);
    
    pthread_join(t1, NULL);
    pthread_join(t2, NULL);

    sem_destroy(&mutex);
    return 0;

}

#include <pthread.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <ctype.h>

//Thread function to create sum of N numbers
int sum = 0;

void* sum_runner(void *arg)
{
    int i;
    int *limit_ptr;
    limit_ptr = (int*) arg;
    int limit = *limit_ptr;

    for(i = 0; i <= limit; i++){
        sum = sum + i;
    }

    pthread_exit(0);
}


int main(int argc, char **argv)
{
    if(argc < 2) {
        printf("error %s \n", argv[0]);
        exit(-1);
    }

    int limit = atoll(argv[1]);
    
    // Thread id
    pthread_t tid;

    // Thread attribute
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    
    pthread_create(&tid, &attr, sum_runner, &limit); //sum_runner is a function ptr

    // wait untill thread is done
    pthread_join(tid, NULL);
    printf("Sum is %d\n", sum);
}

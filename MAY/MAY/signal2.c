#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <errno.h>

void handler_func(int sig_num) 
{
    printf("WHOOO HOOO.....signal was sent between parent and child using SIGUSR1........\n");

}

int main()
{
    pid_t PROCESS;
    int role = -1, status;
    PROCESS = fork();

    if(PROCESS > 0) {
        
        printf("INSIDE PARENT PROCESS HAVING PID %d\n", (int)getpid() );
        signal(SIGUSR1, handler_func);
        role = 0;
        pause();

        printf("WAITING FOR CHILD PROCESS TO BE EXITED\n......");
        PROCESS = wait(&status);
    }

    else if ( PROCESS == 0) {
        printf("INSIDE CHILD HAVING PID %d\n", (int) getpid());

        role = 1;
    
        sleep(1);
        printf("CHILD SENDING SIGNAL USING KILL TO PARENT PROCESS........\n");

        pid_t PPID = getppid();

        kill(PPID, SIGUSR1);
        sleep(2);
    }

    else {
        printf("ERROR IN FRORKING....%d\n", errno);
    }

    printf("%s EXITING.......\n", ((role == 0) ? "Parent" : "child"));

    return 0;
    

}

#include<stdio.h>
#include<signal.h>
#include<unistd.h>
#include<sys/types.h>



void caller_func( int sig_num )
{
    printf("CAUGHT A SIGNAl (CNTRL + C)\n");
    fflush(stdout);
    
    return;

}

int main()
{
    signal(SIGINT, caller_func);

    printf("Go ahead......\n");
    pause();
    return 0;

}

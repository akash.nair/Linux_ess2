#include<stdio.h>
#include<string.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>
 
#define NUM_LOOP 3

pthread_t tid[3];
int counter, timer;
pthread_mutex_t lock;
 
void* trythis(void *arg)
{
    pthread_mutex_lock(&lock);
 
    unsigned long i = 0;
    counter += 1;
    timer += 10; 
    printf("\nTHREAD %d HAS STARTED\n", counter);
    
 //   for(i = 0; i < (NUM_LOOP); i++);
        printf("\nWAITING FOR %d SECONDS TO COMPLETE FOR THREAD %d\n", timer, counter);
        sleep(10);
        printf("\nTHREAD %d HAS FINISHED\n", counter);
        pthread_mutex_unlock(&lock);
    
    return NULL;
}



 
int main(void)
{
    int i = 0;
    int error;
 
    if (pthread_mutex_init(&lock, NULL) != 0) {
        printf("\n mutex init has failed\n");
        return 1;
    }
 
    while(i < 3) {
        error = pthread_create(&(tid[i]), NULL, &trythis, NULL);
        if (error != 0)
            printf("\nThread can't be created :[%s]", strerror(error));
        i++;
    }
 
    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);
    pthread_join(tid[2], NULL);
    pthread_mutex_destroy(&lock);
 
    return 0;
}



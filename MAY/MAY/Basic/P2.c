#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>
#include <stdlib.h>

#define MAXTHREAD 4


/*STEP:2 INSIDE PTHREAD_CREATE THEIR IS ARGUMENT RELATED TO CALL THREAD
         FUNCTION DEFINE THAT FUNCTION */
void *threadfunc(void *arg)
{
    printf("Thread : %d started\n",(int*) arg);

    pthread_exit(arg);
}

int main()
{   
    int i, status, ret;
    pthread_t threadID[MAXTHREAD];

/* STEP:1 CREATE THREAD AND CHECK CREATED OR NOT , IF RETURNS NON- ZERO THEN
   ERROR IN CREATION */

    for(i = 0; i < MAXTHREAD; i++) {
        ret = pthread_create(&threadID[MAXTHREAD], NULL, threadfunc, (void *)i);
        if(ret != 0) {
            printf("Error in thr creation\n");
        }
    }

/* STEP:3 JOIN THE CREATED THREAD TO WAIT AND EXECUTE THREAD FUNCTION BY THREAD
    JOIN*/

    for(i = 0; i < MAXTHREAD; i++) {
        ret = pthread_join(threadID[MAXTHREAD], (void **)&status);
        if(ret != 0) {
            printf("error in joining thread\n");
        }
        else {
            printf("status = %d\n", status);
        }
    }
    return 0;
}

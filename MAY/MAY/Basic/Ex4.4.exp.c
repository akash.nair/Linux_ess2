#include<stdio.h>
#include<pthread.h>
#include<unistd.h>
#include<errno.h>
#include<ctype.h>
#include<assert.h>

#define MAX_THR 2 
#define NUM_LOOP 1000

int sum = 0;
int value = 1;
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;


void* counting_function(void *argp)
{
    FILE *fp;    
    int i;
    int *mytid = (int *)argp; //type casting and de-referencing
    int loop1;
    
    fp = fopen("/home/akash/A/textnew.txt", "a");
    for(loop1 = 1; loop1 <= NUM_LOOP; loop1++) {
        pthread_mutex_lock(&lock);                                                              
        sum += value;                                                                             
        pthread_mutex_unlock(&lock);
        if(*mytid == 1) {
            fprintf(fp, "THR_ID1 = $%d value : %d\n", *mytid, loop1);
        }
        if(*mytid == 2) {
            fprintf(fp, "THR_ID2 = #%d value : %d\n", *mytid, loop1);
        }
    }
    fclose(fp);
}

int main()
{
    int loop;
    pthread_t TID[MAX_THR];
    int count = 1;
    
    for(loop = 1; loop <= MAX_THR; loop++) {
        loop = count;
        count++;
        pthread_create(&TID[loop], NULL, counting_function, &loop);                                         
    }

    for(loop = 1; loop <= MAX_THR; loop++) {
        pthread_join(TID[loop], NULL);
    }
    printf("%d\n", sum);
   // pthread_mutex_destroy(&lock);
    return 0;    
}

#include<stdio.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<string.h>
#include<stdlib.h>


int main()
{
    // 2 pipes needed : 1) To send input from parent 
    //                  2) second to send processed work from child to parent
    
    int fd1[2]; // To store 2 ends of First Pipe
    int fd2[2]; // To store 2 ends of Second pipe

//    char fixed_str[] = "Akash";
//    char input_str[100];
    int num;

    pid_t PROCESS;

    if(pipe(fd1) == -1) {
        fprintf(stderr, "PIPE FAILURE\n");
        return 1;
    }

    if(pipe(fd2) == -1) {
        fprintf(stderr, "PIPE FAILURE\n");
        return 1;
    }

     //printf("enter string to concatenate \n");
    printf("Enter num to be squared \n");
    scanf("%d", &num);
    PROCESS = fork();

/*  IN PARENT PROCESS 
 *  1) CLOSE READING END (fd1(0)).
 *  2) WRITE THROUGH WRITING END (fd1(1)) and close it also.
 *  3) WAIT UNTILL CHILD PROCESS IS FINISHED.
 *  4) PARENT WILL CLOSE WRITING END (fd2(1)).
 *  5) READS THROUGH (fd2(0)).
 *
 * */

    if(PROCESS > 0) {
    //    char concat_str[100];
        int sum;
        close(fd1[0]);

        write(fd1[1], &num, 5);
        close(fd1[1]);

/* WAIT FOR CHILD TO DO IT'S PROCESS */
    
    wait(NULL);
    close(fd2[1]);

/* READ THROUGH FD2[0] PIPE*/
    
    read(fd2[0], &sum, 100);
    printf("Squared number is %d\n", sum);
    close(fd2[0]);
    }

/* IN CHILD PROCESS 
 * CHILD READS THE PROCESS TO BE DONE BY SENT BY PARENT PROCESS 
 * BY CLOSING WRITING END OF PIPE(fd1[1])
 * AFTER PROCESS : PASSES PROCESS VIA fd2 PIPE AND WILL EXIT
* */
    
    else {
        close(fd1[1]);   
        // Read a string using first pipe
       // char concat_str[100];
        int sum;
        read(fd1[0], &sum, 100);

        int i;
    sum = 0;
    for(i = 0; i < num; i++) {
        sum = sum + num;
    }


        /* CLOSE BOTH READING ENDS */
        close(fd1[0]);
        close(fd2[0]);

        /* CONCATENATE AND CLOSE WRITING AND BEFORE IT WRITE TO PARENT */
        write(fd2[1], &sum, 100);  
        close(fd2[1]);
    }
}

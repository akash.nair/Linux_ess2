#include<pthread.h>
#include<sys/types.h>
#include<unistd.h>
#include<stdio.h>
#include<errno.h>
#include<stdlib.h>
#include<string.h>

void *thread1(void *arg)
{
    pthread_t tid;
    tid = pthread_self();
    printf("Thread ran successfully with ID %x\n", (int) tid);

    pthread_exit(NULL);
}

int main()
{
    int ret;
    pthread_t mythread;

    ret = pthread_create(&mythread, NULL, thread1, NULL);

   pthread_join(mythread, NULL);

    if(ret != 0) {
        printf("thread not created %s\n",strerror(errno));
        exit(-1);
    }
    printf("value in ret : %x\n", (int) ret);
    return 0;
}



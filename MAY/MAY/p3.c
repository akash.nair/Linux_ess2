#include <pthread.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <ctype.h>

//Thread function to create sum of N numbers
struct mul_run 
{   
    int limit;
    int answer;
};

void* mul_runner(void *arg)
{
    int i, j, k, A[i][j], B[i][j], C[i][j];
    struct mul_run *arg_ptr = (struct mul_run*) arg;
 
    int mul;
    
    for(i = 0; i < 2; i++) {
        for(j = 0; j < 2; j++) {
            mul = 0;
            for(k = 0; k < 2; k++) {
                mul = mul + A[i][k]*B[k][j];
            }
            C[i][j] = mul;
        }
    }

    printf("MUL OF 2x2 MATRIX\n");
    for(i = 0; i < 2; i++) {
        for(j = 0; j < 2; j++) {
            printf("\nC[%d][%d]:%d", i, j, C[i][j]);
        }
    }
    printf("\n");
    for(i = 0; i < 2; i++) {
        for(j = 0; j < 2; j++) {
            arg_ptr->answer = C[i][j];
        }
    }
    pthread_exit(0);
}

int main(int argc, char **argv)
{
    if(argc < 2) {
        printf("error %s \n", argv[0]);
        exit(-1);
    }

    int num_args = argc - 1;
    struct mul_run args[num_args];
    
    // Thread id
    pthread_t tid[num_args];

    int A[num_args][num_args], B[num_args][num_args], C[num_args][num_args];
    int i, j, k, mul, num = 0;
    printf("Enter MATRIX A\n");
    
    for(i = 0; i < 2; i++) {
        for(j = 0; j < 2; j++) {
            printf("A[%d][%d]:", i, j);
            scanf("\n%d", &A[i][j]);
        }
    }
    printf("\n"); 
    printf("Enter MATRIX B\n");
    
    for(i = 0; i < 2; i++) {
        for(j = 0; j < 2; j++) {
            printf("B[%d][%d]:", i, j);
            scanf("\n%d", &B[i][j]);
        }
    }
    printf("\n");    
    

    // Thread attribute
    for(i = 0; i < num_args; i++) {
        args[i].limit = atoi(argv[i + 1]);
    
        pthread_attr_t attr;
        pthread_attr_init(&attr);
        pthread_create(&tid[i], &attr, mul_runner, &args[i]); //mul_runner is a function ptr
    }

    // wait untill thread is done
    for(i = 0; i < num_args; i++) {
        pthread_join(tid[i], NULL);
        printf("MUL  %d\n", args[i].answer);
    }
}

#include <pthread.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <ctype.h>

void make_matrix();

int main()
{
    make_matrix();
    return 0;
}

void make_matrix()
{
    int A[2][2], B[2][2], C[2][2];
    int i, j, k, mul, num = 0;
    printf("Enter MATRIX A\n");
    
    for(i = 0; i < 2; i++) {
        for(j = 0; j < 2; j++) {
            printf("A[%d][%d]:", i, j);
            scanf("\n%d", &A[i][j]);
        }
    }
    printf("\n"); 
    printf("Enter MATRIX B\n");
    
    for(i = 0; i < 2; i++) {
        for(j = 0; j < 2; j++) {
            printf("B[%d][%d]:", i, j);
            scanf("\n%d", &B[i][j]);
        }
    }
    printf("\n");   

    /*MATRIX MULTIPLICATION*/



    for(i = 0; i < 2; i++) {
        for(j = 0; j < 2; j++) {
            mul = 0;
            for(k = 0; k < 2; k++) {
                mul = mul + A[i][k]*B[k][j];
            }
            C[i][j] = mul;
        }
    }

    printf("MUL OF 2x2 MATRIX\n");
    for(i = 0; i < 2; i++) {
        for(j = 0; j < 2; j++) {
            printf("\nC[%d][%d]:%d", i, j, C[i][j]);
        }
    }
    printf("\n");
}


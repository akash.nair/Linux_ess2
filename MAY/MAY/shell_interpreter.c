#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>

#define MAXLINE 100

int main()
{
    pid_t PROCESS;
    char *ret_fgets;
    char store[MAXLINE + 1];
    int ret_strncmp, status;

    while(1) {
        printf("akash@akashNair$$$");
        ret_fgets = fgets(store, sizeof(store), stdin);

        if (ret_fgets == NULL) {
            exit(-1);
        }
 
        /*  MAIN LINE OF PROGRAM */
        store[strlen(store) - 1] = 0;
        
        if(!strncmp(store, "bye", 3)) exit(0);

        PROCESS = fork();

        if(PROCESS == 0) {
            execlp(store, store, NULL);
        }
        else if(PROCESS > 0) {
            waitpid(PROCESS, &status, 0);
        }

    }
    return 0;
}

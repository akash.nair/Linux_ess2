/*copyright(c) <2018>, Volansys Technologies
 * 
 * Description:
 * @file LS2_Exercise5-2.c	
 * 
 *2.Develop a parent-child process application, wherein child process calculates square of numbers (using progressive arithmetic i.e. square of 2 = 2 
 *+2, square of 3 = 3+3+3 and so on) from 1 to 1000 and parent process prints result as soon as soon as child process produce it. Use Pipes for IPC
 *
 *
 * Author       - Aakash S Sharma
 *
 *******************************************************************************
 *
 * History
 *
 *
 *                      
 * May/7/2018, Aakash S, Created (description)
 *
 ******************************************************************************/
 /*******************
 * Includes
 *******************/  
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
/*************************
 * Defines
 *************************/
/*None*/
/***********************
 * Global Variables
 ***********************/
/*None*/
/************************
 * Function Prototypes
 ************************/
/*None*/
//////MAIN//////
int main() {
  int  pfd[2];     				//creates a pipe for sending and receiving between parent and child
  int  pid[2],i,j,k,m,data=0;     		//pid for identifying different processes and other variables for looping purpose and sendign data
  int  status;                 								   //takes the return status for the waitpid
  if (pipe(pfd) < 0) {                                                                    //Checking that the pipe is created sucessfully
    			fprintf(stderr, "Error creating pipe.\n");
    			exit(0);
  		     }
  
  for(i=1;i<=1000;i++){                                                                      //creates processes and calculates Square values
 		   if ((pid[i] = fork())== 0) { data=i;
                                                for(m=1;m<i;m++){
                                                                    data=data+i;                				
                                                                 }
                                                close(pfd[0]);
    						write(pfd[1], &data, sizeof(data));      //writing the square to one end of the pipe
                                                printf("Child given value for %d time is:%d\n",i,data);
                                                exit(0);

  					       }
                                            sleep(1);					      

					      
  		  }
  for(k=1;k<1002;k++){                                           //The parent processes waiting for the child to complete it's execution of all
  			waitpid(pid[k],&status,3);
                        close(pfd[1]);
  			read(pfd[0],&j, sizeof(j));         //reading pipe values one after another
  			printf("\nThe Square of %d is:-%d",k,j);
			sleep(1);
 		  }
  return 0;
}

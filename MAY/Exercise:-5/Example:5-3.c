/*copyright(c) <2018>, Volansys Technologies
 * 
 * Description:
 * @file LS2_Exercise5-5.c	
 * 
 *Develop a parent-child process application, wherein child process at random duration calculates square of numbers (via above method) from 1 to 1000 
 *and parent process prints result as soon as soon as child process produce it. Use Shared-memory for IPC
 *
 *
 * Author       - Aakash S Sharma
 *
 *******************************************************************************
 *
 * History
 *
 *
 *                      
 * May/8/2018, Aakash S, Created (description)
 *
 ******************************************************************************/
 /*******************
 * Includes
 *******************/  
#include<stdio.h>
#include<sys/shm.h>
#include<string.h>
#include<pthread.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include<semaphore.h>
#include<stdlib.h>
/*************************
 * Defines
 *************************/
/*None*/
/***********************
 * Global Variables
 ***********************/
void *mem;           //pointer to the shared memory space of process
sem_t s;             //Taken  a semaphore variable
/************************
 * Function Prototypes
 ************************/
/*None*/
/////////////MAIN//////////
int main()
{
int shmid, ret,status,i,m,k,data=0;                                                   //variables for status and counting square values.
sem_init(&s, 1, 2);                                                                   //Initalizing Semaphore as for process and value 2
pid_t pid1[5];
key_t key = ftok("shmfile",65);                                                     //Getting the Unique Key Id
shmid = shmget( key, 4096, 0666 | IPC_CREAT );                                        //creating a shared memory Segment with page size of 4KB
mem =(int *)shmat( shmid, (const void *)0, 0 );                                      //attaching the shared memory with the process
if ((pid1[0] = fork())== 0){ 
		               for(i=0;i<1000;i++){     printf("Child\n");          //The child calculates the square of numbers fr0m 1 to 1000
                                                        sem_wait(&s);
                                                        data=i;
    					                for(m=1;m<i;m++){
                                                                          data=data+i;                				
                                                                         }
    					     		sprintf(mem,"%d",data);
							sem_post(&s);
				                        sleep(1);
              					    }
                           }
 

else{                                                                       //The parent will pickup the data from the shared memory given by child
sleep(1);
for(k=1;k<1000;k++){
		     sem_wait(&s);
                     printf("\nThe  square of %d is:-%s\n",k,(char *)mem);
		     sleep(1);
                     sem_post(&s);    
}
waitpid(pid1[0],&status,3);
ret=shmdt(mem);
printf("The returned status is:-%d\n",ret);}
return 0;
}


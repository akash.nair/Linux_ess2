/*copyright(c) <2018>, Volansys Technologies
 * 
 * Description:
 * @file LS2_Exercise5-2.c	
 * 
 *Develop a simple C based shell, which continuously scans for user input and accordingly execute commands. This shell should validate user inputs. 
 *Extra: How would you extend this shell so it provides “|” (Linux pipe) feature?
 *
 *
 * Author       - Aakash S Sharma
 *
 *******************************************************************************
 *
 * History
 *
 *
 *                      
 * May/7/2018, Aakash S, Created (description)
 *
 ******************************************************************************/
 /*******************
 * Includes
 *******************/  
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*************************
 * Defines
 *************************/
#define BUFFER_LEN 100
/***********************
 * Global Variables
 ***********************/
/*None*/
/************************
 * Function Prototypes
 ************************/
/*None*/
//////MAIN//////
int main(){
	char line[BUFFER_LEN];  //get command line
	char* argv[100];        //user command
	char* path= "/bin/";    //set path at bin
	char progpath[20];      //full file path
	int argc;               //arg count

	while(1){

		printf("My shell>> ");                   
		if(!fgets(line, BUFFER_LEN, stdin)){                                      //get command and put it in line
			break;                                                             //if user hits CTRL+D break
		}
		line[ strlen(line)-1] = 0;                                               //fgets takes extra \n at end so remove that
		if(strcmp(line, "exit")==0){                                             //check if command is exit
			break;
		}
		char *token;                                                            //split command into separate strings to take into argv array
		token = strtok(line," ");
		int i=0;
		while(token!=NULL){
			argv[i]=token;      
			token = strtok(NULL," ");
			i++;
		}
		argv[i]=NULL;                                                           //set last value to NULL for execvp
		argc=i;                           
		for(i=0; i<argc; i++){
			printf("%s\n", argv[i]);                                         //print command/args
		}
		strcpy(progpath, path);                                             //copy /bin/ to file path
		strcat(progpath, argv[0]);                                        //add program to path

		for(i=0; i<strlen(progpath); i++){                           //delete newline from the path if there any
			if(progpath[i]=='\n'){      
				progpath[i]='\0';
			}
		}
		int pid= fork();             
		if(pid==0){               
			execvp(progpath,argv);
			fprintf(stderr, "Child process could not do execvp\n");

		}else{                   
			wait(NULL);
			printf("Child exited\n");
		}
	}
}

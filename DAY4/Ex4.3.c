/***************************************************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file	sleeping_barber.c
 * @brief:	(The Sleeping-Barber Problem. A barbershop consists of a waiting room with n chairs, 
 *           and the barber room containing the barber chair. If there are no customers to be 
 *           server, the barber goes to sleep. If a customer enters the barbershop and all chairs 
 *           are occupied, then the customer leaves the shop. If the barber is busy, but chairs are
 *           available, then the customer sits in one of the free chairs. If the barber is asleep, 
 *           the customer wakes up the barber. Write a pseudo program to co-ordinate the barber and
 *           the customers.)
 *
 * @Author       - Akash Nair
 * @bug		 - no known bugs
 ***************************************************************************************************
 * History
 *
 * May/15/2018, Akash , Created (description)
 **************************************************************************************************/

#include "sleeping_barber.h"

int main()
{
	pthread_t chair;
	int ret,status;
	printf("FOR CUSTUMER  PRESS CTRL+Z \n");
	printf("TO EXIT THE PROCESS PRESS CTRL+C\n");
	signal(SIGTSTP,signal_handler);

	while(1) {
	  if(no_of_seat == NO_OF_CHAIRS) {
		printf("BARBER GOING TO SLEEP!.......\n");
		printf("BARBER IS SLEEPING....\n");
		sleep(1);
	  }

	  else if((no_of_seat < NO_OF_CHAIRS) && (no_of_seat > 0)) {
		ret = pthread_create(&chair,NULL,threadfun,(void*)(size_t)no_of_seat);

	    if(ret != 0) {
		  perror("THREAD ERROR\n");
		}

		ret = pthread_join(chair,(void**)&status);
	    
        if(ret != 0) {
		  perror("ERROR JOINING THREAD\n");
		}
	  
      }

	}

	ret = pthread_mutex_destroy(&mymutex);
	
    if(ret != 0) {
	  perror("MUTEX DESTROY ERROR\n");
	}
	
    return 0;
}

void* threadfun(void *arg)                                                                          
{                                                                                                   
    pthread_t id;                                                                                   
    id = pthread_self();                                                                            
    
    assert(pthread_mutex_lock(&mymutex) == 0);                                                      
    printf("BARBER WORKING %d !\n",(int)(size_t)arg);                                     
    
    sleep(3);                                                                                       
    
    printf("%d CUSTOMER GOES...!\n",(int)(size_t)arg);                                      
    no_of_seat++;                                                                                   
    assert(pthread_mutex_unlock(&mymutex) == 0);                                                    
    pthread_exit(NULL);                                                                             
}                                                                                                   
                                                                                                    
void signal_handler(int sig_num)                                                                    
{                                                                                                   
    if(sig_num == 20) {                                                                             
      printf("A CUSTOMER IS  THERE....!\n");                                                    
      
      if(no_of_seat == 0) {                                                                         
        printf("SEAT AVAILIBILITY 0!...!\n");                          
      }                                                                                             
      
      else {                                                                                        
        printf("CUSTOMER HAVING %d SEAT FOR CUTTING....!\n",no_of_seat);                          
        printf("NUMBER OF FREE SEATS :%d\n",no_of_seat - 1);                                     
        no_of_seat--;                                                                               
      }                                                                                             
    }                                                                                               
}          

/***********************************************************************************
 * @file sleeping_barber.h
 * @brief (Function prototype for thread handling and signal handling functions.)
 * 
 * These contains prototypes for various function of thread functions and 
 * eventually any macros, constsants, or global variables you need.
 * 
 * @author 	-	Akash Nair
 * @bug 	-	No known bugs
 ***********************************************************************************/

/*****************
 *Includes
********************/ 
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <stdlib.h>
#include <assert.h>
#include <signal.h>

/**************************
 *Defines
 **********************/
 #define NO_OF_CHAIRS 10   
 
/****************************
* GLOBAL VARIABLES
* ************************/
pthread_mutex_t mymutex = PTHREAD_MUTEX_INITIALIZER;                                                
int no_of_seat = NO_OF_CHAIRS;                                                                      
int free_seat;

/**********************
 *Function prototype
 ************************/
/*********************************************************************
 *@brief (This function to make thread function active.) 
 *
 *@param void (* arg(argument))
 *
 *@return (void pointer is returned in this.)
 *******************************************************************/
void* threadfun(void *arg);

/*************************************************************************************************
 * @brief (This function is used for handling SIGTSTP signal.) 
 * @param int  (int value parameter which contains signal number.)
 * @return void (It returns a void)
 ************************************************************************************************/
void signal_handler(int);

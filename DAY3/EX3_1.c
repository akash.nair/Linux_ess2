/**************************************************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file	 Ex3_1.c
 * @brief:	Develop a program to construct a binary search tree.
 *		    information to understand difference between different types of search.
 *          Develop a multi-threaded application to perform 3X3 matrix
 *          multiplication, each cell of resultant matrix needs to be
 *          calculated in separate thread.
 *
 *
 * @Author       - Akash Nair
 * @bug		     - no bugs
 ***************************************************************************************************
 *
 * History
 *
 *                      
 * MAY/07/2018, Akash , Created (description)
 *
 **************************************************************************************************/
#include "EX3_1.h"

void* multi(void *arg)
{
    int mul;   
    int loop1, loop2 , loop3;    
    int core = step++;
    for(loop1 = 0; loop1 < MAX; loop1++) {
   	    for (int loop2 = 0; loop2 < MAX; loop2++)  {
		    mul = 0;
            for(loop3 = 0; loop3 < MAX; loop3++) {
        	    mul = mul + MATRIX1[loop1][loop3]*MATRIX2[loop3][loop2];
            }
        RESULTANT_MATRIX[loop1][loop2] = mul;
        }
	}
} 


int main()
{
/*Create MATRIX1 and MATRIX2 matrix*/
    int loop1, loop2, loop3, mul, num = 0;
    printf("Enter MATRIX MATRIX1\n");
    
    for(loop1 = 0; loop1 < MAX; loop1++) {
        for(loop2 = 0; loop2 < MAX; loop2++) {
            printf("MATRIX1[%d][%d]:", loop1, loop2);
            scanf("\n%d", &MATRIX1[loop1][loop2]);
        }
    }
    printf("\n"); 
    printf("Enter MATRIX MATRIX2\n");
    
    for(loop1 = 0; loop1 < MAX; loop1++) {
        for(loop2 = 0; loop2 < MAX; loop2++) {
            printf("MATRIX2[%d][%d]:", loop1, loop2);
            scanf("\n%d", &MATRIX2[loop1][loop2]);
        }
    }
    printf("\n"); 
    
/*Declare 4 THREADS*/
    pthread_t threads[MAX_THREAD];
    
/*Create THREAD each evaluating each matrix of its own*/ 
    for(loop1 = 0; loop1 < MAX_THREAD; loop1++) {
        int *p;
        pthread_create(&threads[loop1], NULL, multi, (void*)(p));
    }
/*JOIN AND WAIT THREAD*/   
    for(loop1 = 0; loop1 < MAX_THREAD; loop1++) {
        pthread_join(threads[loop1], NULL);    
    }
/*Displaying result of matrix multiplication*/
       for(loop1 = 0; loop1 < MAX; loop1++) {                                                                        
            for(loop2 = 0; loop2 < MAX; loop2++) {                                                                    
                printf("\nRESULTANT_MATRIX[%d][%d]:%d", loop1, loop2, RESULTANT_MATRIX[loop1][loop2]);                                                
            }                                                                                           
       }
       printf("\n");
    return 0;  
}

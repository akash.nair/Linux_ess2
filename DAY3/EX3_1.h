/*****************************************************************************
 * @file EX3_1.h
 * @brief (Function prototype for varioous thread dunctions)
 * 
 * These contains prototypes for various function of thread functions and 
 * eventually any macros, constsants, or global variables you need.
 * 
 * @author Akash Nair
 * @bug No known bugs.
 *****************************************************************************/

/*****************
 *Includes
********************/ 
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>
#include <string.h>
#include <ctype.h>

/**************************
 *Defines
 **********************/
#define MAX 3
#define MAX_THREAD 3
 
/****************************
* GLOBAL VARIABLES
* ************************/
int MATRIX1[MAX][MAX], MATRIX2[MAX][MAX], RESULTANT_MATRIX[MAX][MAX];
int step = 0;


/*********************************************************************
 *@brief (This function to make thread function active.) 
 *
 *@param void (* arg(argument))
 *
 *@return (void pointer is returned in this.)
 *******************************************************************/
void* multi(void *arg);


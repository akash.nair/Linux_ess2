/**************************************************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file	 Ex2_1.c
 * @brief    Write a C program that will create five directory named a, b, c,
 *             d, e into existing directory using linux system calls
 *
 *
 * @Author       - Akash Nair
 * @bug		     - no bugs
 ***************************************************************************************************
 *
 * History
 *
 *                      
 * MAY/04/2018, Akash , Created (description)
 *
 **************************************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<unistd.h>


int spawn (char* program, char** arg_list)
{
	pid_t child_pid;
	child_pid = fork ();
	if (child_pid != 0)
		return child_pid;
	else {
		execvp (program, arg_list);
		fprintf (stderr, "an error occurred in execvp\n");
		abort ();
	}
}
int main ()
{
	char* arg_list[] = {
		"mkdir",
		"a",
		"b",
		"c",
		"d",
		"e",
		NULL};
			spawn ("mkdir", arg_list);
	printf ("done with main program\n");
	return 0;
}

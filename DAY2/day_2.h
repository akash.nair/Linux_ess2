/*****************************************************************************
 * @file EX2_1.h
 * @brief (Function prototype for varioous thread dunctions)
 * 
 * These contains prototypes for various function of thread functions and 
 * eventually any macros, constsants, or global variables you need.
 * 
 * @author Akash Nair
 * @bug No known bugs.
 *****************************************************************************/

/*****************
 *Includes
********************/ 
#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<unistd.h>

/******************
* Defines
******************/
 
 /*NONE*/


/*********************************************************************
 *@brief (This function to make thread function active.)
          Spawn a child process running a new program. PROGRAM is the name
          of the program to run; the path will be searched for this program.
          ARG_LIST is a NULL-terminated list of character strings to be
          passed as the program’s argument list. Returns the process ID of
          the spawned process. 

 *@param void  (char *, char ** arg_list)
 *
 *@return      ( int is returned)
 *******************************************************************/

int spawn (char* program, char** arg_list);

